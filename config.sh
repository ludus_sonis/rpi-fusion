dtb=my-overlay.dtb
devices="lsm303dlhc_accel lsm303dlhc_magn l3g4200d"

more_load() {
	sudo mkdir -p /sys/kernel/config/device-tree/overlays/dofs
	sudo cp ${dtb} /sys/kernel/config/device-tree/overlays/dofs/dtbo
	sleep 1
	sudo modprobe iio-trig-hrtimer
	sudo mkdir /sys/kernel/config/iio/triggers/hrtimer/lsm303dlhc_magn-trigger
}

lsm303dlhc_accel_scan_elements="accel_x accel_y accel_z"
lsm303dlhc_accel_samplerate=50

lsm303dlhc_magn_scan_elements="magn_x magn_y magn_z"
lsm303dlhc_magn_samplerate=15
lsm303dlhc_magn_more_enable() {
	for trigger in /sys/bus/iio/devices/trigger*; do
		read name < ${trigger}/name
		if [ "${name}" = "lsm303dlhc_magn-trigger" ]; then
			echo ${name} > ${lsm303dlhc_magn_iiodev}/trigger/current_trigger
			echo $((lsm303dlhc_magn_samplerate - 1)) > ${trigger}/sampling_frequency
			return
		fi
	done
	echo "Error lsm303dlhc_magn couln't find hrtrigger, exiting"
	exit 1
}

l3g4200d_scan_elements="anglvel_x anglvel_y anglvel_z timestamp"
l3g4200d_samplerate=100
