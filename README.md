Fusion using fusion-rs on raspberry pi with lsm303dlhc and l3g4200d.

config.sh can be configured for samplerates (only)
and config.toml for variances

10-iio.rules must be placed in /etc/udev/rules.d/

window A:
. ./scripts/helper_script
window B (for example):
cargo run --release -- --dump_rpy
window A:
enable
