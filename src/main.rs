use std::io::Result;

use fusion::{
    fusion::Fusion,
    devsampler::DevSampler,
    samplers::{lsm303dlhc_accel::*, lsm303dlhc_magn::*, l3g4200d::*},
};

fn main() -> Result<()> {
    let lsm_acc_ds = DevSampler::from_dst(Lsm303dlhcAccel::new(), "/dev/accelerometer")?;
    let lsm_magn_ds = DevSampler::from_dst(Lsm303dlhcMagn::new(), "/dev/magnetometer")?;
    let l3g_ds = DevSampler::from_dst(L3g4200d::new(), "/dev/gyroscope")?;

    Fusion::with_args([lsm_acc_ds, lsm_magn_ds, l3g_ds].into())
}
